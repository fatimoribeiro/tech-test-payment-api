using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Persistence
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<VendaItem> VendaItens { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Venda>(tabela => {
                tabela.HasKey(e => e.Id);
                tabela
                    .HasMany(e => e.VendaItens)
                    .WithOne()
                    .HasForeignKey(i => i.VendaId);
            });

            builder.Entity<VendaItem>(tabela => {
                tabela.HasKey(e => e.Id);
            });
        }
    }
}