using System.ComponentModel;

namespace tech_test_payment_api.Models;
public class VendaItem
{
    [DefaultValue("1")]
    public int Id { get; set; }
    
    [DefaultValue("Produto 1")]
    public string? Nome { get; set; }
    
    public int VendaId { get; set; }

    public VendaItem()
    {        
    }
}