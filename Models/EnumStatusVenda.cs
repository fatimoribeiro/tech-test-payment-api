using System.ComponentModel;

namespace tech_test_payment_api.Models
{
    public enum EnumStatusVenda
    {
        [Description("Aguardando Pagamento")]
        AguardandoPagamento,
        [Description("Pagamento Aprovado")]
        PagamentoAprovado,
        [Description("Enviado para Transportadora")]
        EnviadoParaTransportadora,
        [Description("Entregue")]
        Entregue,
        [Description("Cancelado")]
        Cancelado
    }
}