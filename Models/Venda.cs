using System.ComponentModel;

namespace tech_test_payment_api.Models;

public class Venda
{
    public int Id { get; set; }
    public string? IdentificadorPedido { get; set; }
    public DateTime DataPedido { get; set; }
    public EnumStatusVenda StatusVenda { get; set; }

    [DefaultValue("1")]
    public int IdVendedor { get; set; }
    
    [DefaultValue("Fatimo Ribeiro")]
    public string? NomeVendedor { get; set; }
    
    [DefaultValue("283.308.928-75")]
    public string? CpfVendedor { get; set; }
    
    [DefaultValue("fatimo@vendas.com.br")]
    public string? EmailVendedor { get; set; }
    
    [DefaultValue("11-98765-4321")]
    public string? TelefoneVendedor { get; set; }

    public List<VendaItem> VendaItens { get; set; }

    public Venda()
    {
        this.VendaItens = new List<VendaItem>();
    }
}