using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;
using tech_test_payment_api.Persistence;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private DatabaseContext _context { get; set; }

        public VendaController(DatabaseContext context)
        {
            this._context = context;
        }

        [HttpGet("ObterVendaPorId/{id}")]
        public ActionResult<List<Venda>> GetById([FromRoute] int id)
        {
            var result = _context.Vendas.Include(p => p.VendaItens).ToList();

            if (!result.Any())
                return NotFound(new { msg = "Nenhuma venda encontrada." });

            return Ok(result);
        }

        [HttpPost("CriarVenda")]
        public ActionResult<Venda> Post([FromBody] Venda venda)
        {
            if (venda.DataPedido == DateTime.MinValue)
                return BadRequest(new { msg = "A data do pedido não pode ser vazia." });

            if (venda.IdentificadorPedido is null)
                return BadRequest(new { msg = "O identificador do pedido não pode ser vazio." });

            if (venda.StatusVenda.ToString() is null)
                return BadRequest(new { msg = "O status do pedido não pode ser diferente de AguardandoPagamento." });

            try
            {
                _context.Vendas.Add(venda);
                _context.SaveChanges();
            }
            catch(System.Exception)
            {
                return BadRequest(new { msg = "Erro ao tentar inserir o registro." });
            }
            
            return Created("Registro criado com sucesso", venda);
        }


        [HttpPost("AtualizarStatusVenda/{id}")]
        public ActionResult<Object> Update(int id, EnumStatusVenda status)
        {
            bool bReturn = false;
            string msg = string.Empty;

            var result = _context.Vendas.SingleOrDefault(e => e.Id == id);

            if (result is null) {
                return NotFound(new { msg = "Registro não encontrado." });
            }
            else
            {
                switch(result.StatusVenda)
                {
                    case EnumStatusVenda.AguardandoPagamento:
                    {
                        if ((status == EnumStatusVenda.PagamentoAprovado) || (status == EnumStatusVenda.Cancelado))
                        {
                            bReturn = true;
                            result.StatusVenda = status;
                        }
                        else
                        {
                            msg = "Operação não efetivada. Para uma venda com status 'Aguardando Pagamento', seu status só pode ser alterado para 'Pagamento Aprovado' ou 'Cancelado'.";
                        }
                        break;
                    }                        
                    case EnumStatusVenda.PagamentoAprovado:
                    {
                        if ((status == EnumStatusVenda.EnviadoParaTransportadora) || (status == EnumStatusVenda.Cancelado))
                        {
                            bReturn = true;
                            result.StatusVenda = status;
                        }
                        else
                        {
                            msg = "Operação não efetivada. Para uma venda com status 'Pagamento Aprovado', seu status só pode ser alterado para 'Enviado para Transportadora' ou 'Cancelado'.";
                        }
                        break;
                    }
                    case EnumStatusVenda.EnviadoParaTransportadora:
                    {
                        if (status == EnumStatusVenda.Entregue)
                        {
                            bReturn = true;
                            result.StatusVenda = status;
                        }
                        else
                        {
                            msg = "Operação não efetivada. Para uma venda com status 'Enviado para Transportadora', seu status só pode ser alterado para 'Entregue'.";
                        }
                        break;
                    }
                    case EnumStatusVenda.Entregue:
                    {
                        msg = "Operação não efetivada. O pedido já foi entregue e seu status não pode ser alterado.";
                        break;
                    }
                    default:
                    {
                        msg = "Operação não efetivada. O status do pedido não pode ser vazio.";
                        break;
                    }
                }

                if (bReturn) {
                    _context.SaveChanges();
                    return Ok(new { msg = $"Status da venda {id} atualizado com sucesso." });
                } else {
                    return BadRequest(new { msg });
                }
            }
       }
    }
}